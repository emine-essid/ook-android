package com.interco.e.daggerrxretrofit.ui.home.scan

import android.app.Activity
import android.content.Context
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertNotNull
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import java.util.regex.Pattern


/**
 * Created by emine on 04/01/2019.
 */

class ScanAcitivyPresenterTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private val scanAcitivyView: ScanAcitivyView? = null

    @InjectMocks
    private var scanAcitivyPresenter: ScanAcitivyActivityPresenter? = null

    private var context: Context? = null


    private val ISBN = "8717418099039" //BLUERAY JACK REACHER
    private val DATA: String = "video.fnac.com_a10403367_Alibi-com-Blu-ray-Philippe-Lacheau-Blu-ray?omnsearchpos=1"


    private var activity: Activity? = null

    @Before
    fun setUp() {
        context = mock(Context::class.java)
        activity = mock(Activity::class.java)
       // `when`(scanAcitivyPresenter?.networkApiService).thenReturn(App.getAppComponant().provideApiService())
   //     `when`(scanAcitivyPresenter?.viewCallback).thenReturn(scanAcitivyView)

    }

    @Test
    fun `should return parsingFailed null ISBN `() {
        scanAcitivyPresenter?.fnacScarapper(null, activity)
        verify<ScanAcitivyView>(scanAcitivyView).parsingFailed("*** null EAN ***")
    }

    @Test
    fun `should return Succses  `() {
        assertNotNull(activity)
        assertFalse(ISBN.isEmpty())
        scanAcitivyPresenter?.getData("video.fnac.com_a10403367/" + DATA)
        verify<ScanAcitivyView>(scanAcitivyView).getDataSuccses(ArgumentMatchers.any())
    }


    @Test
    fun `test regex v1  `() {
        val pattern = Pattern.compile("([a-z]+\\.fnac\\.com_).*(a[0-9]+)")
        val matcher = pattern.matcher(DATA)
        var ch: String = ""
        while (matcher.find()) {
            ch += matcher.group()
        }
        Assert.assertFalse(ch.isEmpty())

    }


}


//************************* Custom RX rules ********************

/*
  @Rule @JvmField
val rule = MockitoJUnit.rule()!!

@Rule
@JvmField var testSchedulerRule = RxImmediateSchedulerRule()

 */

class RxImmediateSchedulerRule : TestRule {

    override fun apply(base: Statement, d: Description): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
                RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
                RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
                RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

                try {
                    base.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
    }
}
