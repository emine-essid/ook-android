package com.interco.e.daggerrxretrofit.ui.home.login

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.interco.e.daggerrxretrofit.BuildConfig
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by emine on 04/01/2019.
 */
@RunWith(MockitoJUnitRunner::class)
class GooleAthActivityPresenterTest {

    private val ADMIN_ACCOUNT = BuildConfig.Admin_Login
    private val LAMBDA_ACCOUNT = "test.account@soat.fr"
    private var fakeAccount: GoogleSignInAccount? = null


    @Mock
    private var gooleAthActivity: GooleAthActivity? = null
    @InjectMocks
    private var gooleAthActivityPresenter: GooleAthActivityActivityPresenter? = null

    @Before
    fun setUp() {
        fakeAccount = GoogleSignInAccount.createDefault()
    }

    @Test
    fun `should Call Error Message With error message `() {
        assertNotNull(gooleAthActivityPresenter)
        gooleAthActivityPresenter!!.shouldIDPass(null)
        verify<GooleAthActivity>(gooleAthActivity).toggleLoading(true)
        verify<GooleAthActivity>(gooleAthActivity).showError(anyString())


    }

    @Test
    fun `should Call ErrorMessage With String_EMAINNULL`() {
        gooleAthActivityPresenter!!.shouldIDPass(fakeAccount!!.email)
        verify<GooleAthActivityViewInterface>(gooleAthActivity).toggleLoading(true)
        verify<GooleAthActivityViewInterface>(gooleAthActivity).showError(anyString())
    }

    @Test
    fun `should Call Isadmin User`() {
        gooleAthActivityPresenter!!.shouldIDPass(ADMIN_ACCOUNT)
        verify<GooleAthActivity>(gooleAthActivity).toggleLoading(true)
        verify<GooleAthActivity>(gooleAthActivity).isAdmin(ADMIN_ACCOUNT)
    }

    @Test
    fun `should Call IsLambda User`() {
        gooleAthActivityPresenter!!.shouldIDPass(LAMBDA_ACCOUNT)
        verify<GooleAthActivity>(gooleAthActivity).toggleLoading(true)
        verify<GooleAthActivity>(gooleAthActivity).isLambda(LAMBDA_ACCOUNT)
    }


}