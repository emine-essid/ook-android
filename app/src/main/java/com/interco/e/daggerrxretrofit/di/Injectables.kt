package com.interco.e.daggerrxretrofit.di

import com.interco.e.daggerrxretrofit.di.componant.AppComponant
import com.interco.e.daggerrxretrofit.root.App

/**
 * Created by emine on 20/01/2019.
 */
open class Injectables {
    var appComponant: AppComponant? = null

    init {
        try {
            appComponant = App.appComponant
            appComponant!!.inject(this)
        } catch (T: Exception) {
            //TEST IS RUNNING
        }

    }
}
