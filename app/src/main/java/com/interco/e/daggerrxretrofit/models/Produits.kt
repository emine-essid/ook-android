package com.interco.e.daggerrxretrofit.models

data class Produits(
        val statusCode: String, // 200
        val id: Int, // 1043
        val createdDate: String, // null
        val updatedDate: String, // null
        val createdBy: String, // null
        val modifiedBy: String, // null
        val barCode: String, // 8717418434434
        val title: String, // Jack Reacher : Never Go Back Blu-ray
        val authors: List<String>,
        val publicationDate: String,
        val totalQuantity: Int, // 1
        val availableQuantity: Int, // 1
        val category: Category,
        val image: String // https://static.fnac-static.com/multimedia/Images/FR/NR/91/13/81/8459153/1540-1/tsp20161230110217/Jack-Reacher-Never-Go-Back
)