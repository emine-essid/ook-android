package com.interco.e.daggerrxretrofit.ui.home.login;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.interco.e.daggerrxretrofit.base.activity.BaseActivityViewInterface;

/**
 * Created by emine on 04/01/2019.
 */
public interface GooleAthActivityViewInterface extends BaseActivityViewInterface {
    void showError(String error);

    void isAdmin(String account);

    void isLambda(String account);

    GoogleSignInAccount getAccount();


}
