package com.interco.e.daggerrxretrofit.ui.home.scan;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.interco.e.daggerrxretrofit.base.presenter.BaseActivityPresenter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by emine on 14/12/2018.
 */
@SuppressLint("CheckResult")
public class ScanAcitivyActivityPresenter extends BaseActivityPresenter<ScanAcitivyView> {


    public void getData(String key) {
        if (key != null && !key.isEmpty()) {


            subscribeMainThred(getNetworkApiService().getRepoFromREmote(key), result -> {
                getViewCallback().getDataSuccses(result);

            }, error -> {
                getViewCallback().getDataFailed(error.getMessage());

            });
        } else {
            getViewCallback().getDataFailed("invalide ISBN");

        }

    }

    public void urlFormatter(String data, String code) {
        try {
            Timber.e("******* URL ----->" + data);


            Pattern pattern = Pattern.compile("([a-z]+\\.fnac\\.com_).*(a[0-9]+)");
            Matcher matcher = pattern.matcher(data);
            String ch = "";
            while (matcher.find()) {
                ch += matcher.group();
            }

            if (!ch.isEmpty()) {
                getData(ch + "/" + code);
            } else {

                getViewCallback().parsingFailed(" \nERROR REGEX -> %s" + matcher.toString());
                Timber.e(" [ MATCHER CONTAIN ]%s", matcher.group().length());

            }


        } catch (Exception p) {
            getViewCallback().parsingFailed(" \nERROR REGEX -> %s" + p.getMessage());
            Timber.e(" [ EXEPTION ]%s", p.getMessage());

        }

    }

    public void fnacScarapper(String EAN, Activity activity) {
        new Thread(() -> {
            final StringBuilder builder = new StringBuilder();
            if (EAN != null) {

                try {
                    Document doc = Jsoup.connect("https://www.fnac.com/SearchResult/ResultList.aspx?Search=" + EAN).get();
                    Elements titre = doc.getElementsByClass("Article-desc");
                    try {
                        builder.append("\n").append(titre.get(0).select("a").first().attr("href"));

                    } catch (Exception e) {
                        getViewCallback().parsingFailed("* parging link of product *" + e.getMessage());
                    }


                } catch (IOException e) {
                    getViewCallback().parsingFailed("* scrapping html failed*");
                }

                activity.runOnUiThread(() -> {

                    if (!builder.toString().isEmpty()) {

                        String result = builder.toString().replace("https://", "")
                                .replace("http://", "")
                                .replace("/", "_");

                        urlFormatter(result, EAN);
                    } else {
                        getViewCallback().parsingFailed("*** string builder is empty or null");
                    }

                });
            } else {
                getViewCallback().parsingFailed("*** null EAN ***");

            }
        }).start();
    }
}
