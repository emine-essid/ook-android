package com.interco.e.daggerrxretrofit.ui.home.scan;

import com.interco.e.daggerrxretrofit.base.BaseViewInterface;
import com.interco.e.daggerrxretrofit.base.activity.BaseActivityViewInterface;
import com.interco.e.daggerrxretrofit.models.Produits;

/**
 * Created by emine on 14/12/2018.
 */

public interface ScanAcitivyView extends BaseActivityViewInterface {

   void getDataSuccses(Produits product);

    void getDataFailed(String error);

    void parsingFailed(String message);
}
