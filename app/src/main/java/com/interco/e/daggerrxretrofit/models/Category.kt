package com.interco.e.daggerrxretrofit.models

data class Category(
        val id: Int, // 3
        val createdDate: Any, // null
        val updatedDate: Any, // null
        val createdBy: Any, // null
        val modifiedBy: Any, // null
        val libelleCategory: String, // films
        val description: String // categorie des film
)