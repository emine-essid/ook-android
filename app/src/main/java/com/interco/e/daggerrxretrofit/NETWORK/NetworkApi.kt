package com.interco.e.daggerrxretrofit.NETWORK

import com.interco.e.daggerrxretrofit.models.Produits
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by emine on 14/12/2018.
 */
interface NetworkApi {
//https://sharp-bobcat-84.localtunnel.me/fnacscrap/video.fnac.com_a2250305

    @Headers("Content-Type: application/json")
    @GET
    fun getscannedProduct(@Url code: String): Observable<Produits>


    @Headers("Content-Type: application/json")
    @POST("/reservation{email}/{ean}")
    fun reserverProduit(@Path("email") email: String, @Path("ean") isbn: String): Observable<Boolean>

}
