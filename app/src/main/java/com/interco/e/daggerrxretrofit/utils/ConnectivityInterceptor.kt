package com.interco.e.daggerrxretrofit.utils

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        if (!isConnectedToNetwork(context)) {
            throw NoConnectivityException()
        } else {
            val response = chain.proceed(chain.request())
            return response
        }
    }

    companion object {

        fun isConnectedToNetwork(context: Context): Boolean {
            lateinit var connectivityManager: ConnectivityManager
            connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            var isConnected = false
            val activeNetwork = connectivityManager.activeNetworkInfo
            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting


            return isConnected
        }
    }
}


internal class NoConnectivityException : IOException() {

    override val message: String?
        get() = "No network available, please check your WiFi or Data connection"
}