package com.interco.e.daggerrxretrofit.base.fragment

import android.os.Bundle
import android.view.View

import androidx.annotation.Keep

import com.interco.e.daggerrxretrofit.base.presenter.BaseActivityPresenter
import com.interco.e.daggerrxretrofit.base.presenter.BasePresenterFragment

@Keep
abstract class BaseFragment<P : BaseActivityPresenter<O>, O : BaseFragmentViewInterface> : BasePresenterFragment<P, O>() {

    val isFragmentRunning: Boolean
        get() = this.isAdded && !this.isDetached && !this.isHidden && !this.isRemoving && !this.activity!!.isFinishing


    public override var viewInterface: O
        get() = super.viewInterface
        set(value: O) {
            super.viewInterface = value
        }

    private fun setViewInterface() {
        this.viewInterface = this.createViewInterface()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}