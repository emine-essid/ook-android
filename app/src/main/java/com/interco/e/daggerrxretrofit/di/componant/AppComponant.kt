package com.interco.e.daggerrxretrofit.di.componant

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

import com.interco.e.daggerrxretrofit.NETWORK.NetworkApiService
import com.interco.e.daggerrxretrofit.di.Injectables
import com.interco.e.daggerrxretrofit.di.module.AppModule
import com.interco.e.daggerrxretrofit.di.scope.AppScope

import dagger.Component

/**
 * Created by emine on 14/12/2018.
 */

@Component(modules = arrayOf(AppModule::class))
@AppScope
interface AppComponant {
    fun provideApiService(): NetworkApiService

    fun provideConext(): Context

    fun sharedPrefsManager(): SharedPreferences

    fun inject(application: Application)

    fun inject(application: Injectables)

}
