package com.interco.e.daggerrxretrofit.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.interco.e.daggerrxretrofit.R
import com.interco.e.daggerrxretrofit.ui.home.scan.ScanAcitivy
import com.interco.e.daggerrxretrofit.utils.DialogUtils


class HomeActivity : AppCompatActivity() {

    val intent_add_product = 1
    val intent_rent_product = 2
    val intent_return_product = 3

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = intent
        val email = intent.getStringExtra("email")
        val isAdmin = intent.getBooleanExtra("isAdmin", false)


        if (isAdmin) {
            setContentView(R.layout.activity_home_admin)
            val addBtn = findViewById<ImageView>(R.id.btn_add)

            addBtn.setOnClickListener { view ->
                val intentOpenScan = Intent(this@HomeActivity, ScanAcitivy::class.java)
                intentOpenScan.putExtra("ajouter", true)
                intentOpenScan.putExtra("email", email)
                startActivityForResult(intentOpenScan, intent_add_product)
            }

        } else {
            setContentView(R.layout.activity_home_lambda)
            val buttonEmprunter = findViewById<ImageView>(R.id.emprunter_icone)
            val buttonRendre = findViewById<ImageView>(R.id.rendre_icu)

            buttonEmprunter.setOnClickListener {//TODO
                view ->
            }

            buttonRendre.setOnClickListener {//TODO
                view ->
            }


        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                DialogUtils.showBottomMessage(this, result, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                val result = data!!.getStringExtra("result")
                DialogUtils.showBottomMessage(this, result, true);

            }
        }
    }
}
