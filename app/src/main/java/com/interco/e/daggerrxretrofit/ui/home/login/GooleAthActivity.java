package com.interco.e.daggerrxretrofit.ui.home.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.interco.e.daggerrxretrofit.R;
import com.interco.e.daggerrxretrofit.base.activity.BaseActivity;
import com.interco.e.daggerrxretrofit.ui.home.HomeActivity;
import com.interco.e.daggerrxretrofit.utils.ConnectivityInterceptor;
import com.interco.e.daggerrxretrofit.utils.DialogUtils;

import timber.log.Timber;

public class GooleAthActivity extends BaseActivity<GooleAthActivityActivityPresenter, GooleAthActivityViewInterface> implements GooleAthActivityViewInterface, View.OnClickListener {

    private static final int RC_SIGN_IN = 9999;
    public GoogleSignInAccount account;
    private GoogleSignInClient mGoogleSignInClient;

    @NonNull
    @Override
    public GooleAthActivityViewInterface createViewInterface() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initviews();
    }

    private void initviews() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        SignInButton signInButton = findViewById(R.id.sign_in_button_gmail);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null)
            mGoogleSignInClient.signOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);
            if (getPresenter() != null)
                getPresenter().shouldIDPass(account.getEmail());
            else
                Timber.e("PRESENTER NULLL !!!!!");
            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Timber.e("signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sign_in_button_gmail) {

            if (!ConnectivityInterceptor.Companion.isConnectedToNetwork(this)) {
                DialogUtils.INSTANCE.showBottomMessage(this, "No network available, please check your WiFi or Data connection", true);
            } else {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }

        }
    }


    @Override
    public void showError(String error) {
        DialogUtils.INSTANCE.showBottomMessage(this, error, true);
    }

    @Override
    public void isAdmin(String accountEmail) {
        Intent intent = new Intent(GooleAthActivity.this, HomeActivity.class);
        intent.putExtra("isAdmin", true);
        intent.putExtra("email", accountEmail);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void isLambda(String accountEmail) {
        Intent intent = new Intent(GooleAthActivity.this, HomeActivity.class);
        intent.putExtra("isAdmin", false);
        intent.putExtra("email", accountEmail);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public GoogleSignInAccount getAccount() {
        return account;
    }


}
