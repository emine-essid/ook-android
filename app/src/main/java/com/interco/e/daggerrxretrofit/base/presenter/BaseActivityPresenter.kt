package com.interco.e.daggerrxretrofit.base.presenter


import com.interco.e.daggerrxretrofit.NETWORK.NetworkApiService
import com.interco.e.daggerrxretrofit.base.BaseViewInterface
import com.interco.e.daggerrxretrofit.base.eventBus.BaseEvent
import com.interco.e.daggerrxretrofit.base.eventBus.RxBus
import com.interco.e.daggerrxretrofit.di.Injectables
import com.interco.e.daggerrxretrofit.utils.PreferencesHelper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.mockito.Mockito
import timber.log.Timber

/**
 * Created by emine on 14/12/2018.
 */
open class BaseActivityPresenter<V : BaseViewInterface> : Injectables() {


    var networkApiService: NetworkApiService
    var preferencesHelper: PreferencesHelper
    var viewCallback: V? = null
        private set

    //RX
    private val subscriptions = CompositeDisposable()
    //RX BUS
    private val busEventSubscriptions = CompositeDisposable()

    init {
        try {
            networkApiService = appComponant!!.provideApiService()
            preferencesHelper = PreferencesHelper(appComponant!!.sharedPrefsManager())

        } catch (e: Exception) {
            //mocking ?
            Timber.e(" mocking Network Service !!!!!%s", e)
            networkApiService = Mockito.mock(NetworkApiService::class.java)
            preferencesHelper = Mockito.mock(PreferencesHelper::class.java)

        }

    }

    fun attachViewCallback(viewCallback: V) {
        this.viewCallback = viewCallback
    }

    fun attachViewCallback(viewCallback: Any) {
        this.viewCallback = viewCallback as V
    }

    //    -------------

    private fun sub(s: Disposable) {
        this.subscriptions.add(s)
    }

    protected fun clearSubs() {
        this.subscriptions.clear()
    }

    protected fun <R> subscribe(observable: Observable<R>, onNext: Consumer<in R>, onError: Consumer<Throwable>, onCompleted: Action) {
        this.sub(observable.subscribe(onNext, onError, onCompleted))
    }

    protected fun <R> subscribeMainThred(observable: Observable<R>, onNext: Consumer<in R>, onError: Consumer<Throwable>?) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError!!)
    }


    protected fun <R> subscribe(observable: Observable<R>, onNext: Consumer<in R>) {
        this.subscribeMainThred(observable, onNext, null)
    }

    protected fun subscribe(completable: Completable, onComplete: Action, onError: Consumer<in Throwable>?) {
        completable.doOnSubscribe { this.sub(it) }.subscribe(onComplete, onError!!)
    }

    protected fun subscribe(completable: Completable, onComplete: Action) {
        this.subscribe(completable, onComplete, null)
    }

    private fun subscribeBusEvents() {
        val observable = RxBus.getInstance().toObservable().doOnNext { this.onBusEventReceived(it) }
        this.busEventSubscriptions.add(observable.subscribeOn(AndroidSchedulers.mainThread()).subscribe())
    }

    private fun unsubscribeBusEvents() {
        this.busEventSubscriptions.clear()
    }

    protected open fun onBusEventReceived(event: BaseEvent) {}

    //--- activity life cycle


    fun onCreated() {
        this.subscribeBusEvents()
    }

    fun onStart() {}

    fun onResume() {}

    fun onPause() {
        this.clearSubs()
    }

    fun onStop() {}

    fun onDestroy() {
        this.unsubscribeBusEvents()
    }


}
