package com.interco.e.daggerrxretrofit.ui.home.login;

import com.interco.e.daggerrxretrofit.BuildConfig;
import com.interco.e.daggerrxretrofit.base.eventBus.BaseEvent;
import com.interco.e.daggerrxretrofit.base.presenter.BaseActivityPresenter;

import timber.log.Timber;

/**
 * Created by emine on 04/01/2019.
 */
public class GooleAthActivityActivityPresenter extends BaseActivityPresenter<GooleAthActivity> {


    public void shouldIDPass(String accountEmail) {
        Timber.e("current admin is %s", BuildConfig.Admin_Login);


        getPreferencesHelper().putString("UserMail", accountEmail);

        getViewCallback().toggleLoading(true);
        if (accountEmail != null && accountEmail.contains("@soat.fr")) {

            if (accountEmail.equals(BuildConfig.Admin_Login)) {
                getViewCallback().isAdmin(accountEmail);
                getViewCallback().toggleLoading(false);
                //ADmin
            } else {
                //Lambda user
                getViewCallback().isLambda(accountEmail);
                getViewCallback().toggleLoading(false);

            }

        } else {
            getViewCallback().showError("Acces denied");
            getViewCallback().toggleLoading(false);

        }
    }


    @Override
    protected void onBusEventReceived(BaseEvent event) {
        super.onBusEventReceived(event);

    }
}
