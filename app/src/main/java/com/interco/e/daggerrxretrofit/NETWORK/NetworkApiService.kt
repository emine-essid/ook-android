package com.interco.e.daggerrxretrofit.NETWORK

import com.google.gson.Gson
import com.interco.e.daggerrxretrofit.models.Produits
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by emine on 14/12/2018.
 */
@Singleton
open class NetworkApiService @Inject
constructor(internal var gson: Gson, internal var retrofit: Retrofit) {
    private val networkApi: NetworkApi

    init {
        networkApi = retrofit.create(NetworkApi::class.java)

    }

    fun getRepoFromREmote(key: String): Observable<Produits> {
        val getProduitScanned = networkApi.getscannedProduct(key)

        return getProduitScanned

    }

    //TODO SOON , ILYES WORKING ON IT !
    fun reserverProduit(email: String, ean: String): Observable<Boolean> {
        val reserverProduit = networkApi.reserverProduit(email, ean)
        return reserverProduit

    }
}
