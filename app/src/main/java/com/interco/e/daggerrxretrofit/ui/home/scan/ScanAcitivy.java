package com.interco.e.daggerrxretrofit.ui.home.scan;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.interco.e.daggerrxretrofit.R;
import com.interco.e.daggerrxretrofit.base.activity.BaseActivity;
import com.interco.e.daggerrxretrofit.models.Produits;
import com.interco.e.daggerrxretrofit.utils.ConnectivityInterceptor;
import com.interco.e.daggerrxretrofit.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;


/**
 *
 */
public class ScanAcitivy extends BaseActivity<ScanAcitivyActivityPresenter, ScanAcitivyView> implements ScanAcitivyView {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private SurfaceView surfaceView;
    private IntentIntegrator integrator;

    @NonNull
    @Override
    public ScanAcitivyView createViewInterface() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        surfaceView = findViewById(R.id.barcodeFragment);


        if (checkPermission()) {
            integrator = new IntentIntegrator(ScanAcitivy.this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setPrompt("scanning Soat item ...");
            integrator.setCameraId(0);
// Use a specific camera of the device
            integrator.setBeepEnabled(true);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();// `this` is the current Activity

        } else {
            requestPermission();
        }
    }


    /**
     * @param product
     */
    @Override
    public void getDataSuccses(Produits product) {
        Intent returnIntent = new Intent();
        Timber.e("***********PRODCUT FOUND !**********%s", product.toString());
        if (product.getStatusCode() == null) {
            returnIntent.putExtra("result", product.getTitle() + " existe dans la base");
        } else {
            returnIntent.putExtra("result", product.getTitle() + " vient d'étre ajouter  dans la base");
        }
        setResult(Activity.RESULT_OK, returnIntent);
        finish();


    }

    /**
     * @param error
     */
    @Override
    public void getDataFailed(String error) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", error);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void parsingFailed(String message) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", message);
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (!ConnectivityInterceptor.Companion.isConnectedToNetwork(this)) {
            DialogUtils.INSTANCE.showBottomMessage(this, "No network available, please check your WiFi or Data connection", true);
        } else {
            if (result != null) {
                if (result.getContents() == null) {
                    finish();
                } else {
                    Timber.e("*** result " + result.toString());
                    //TODO REPLACE BY GET DATA ( NO NEED SCRAPPER )
                    getPresenter().fnacScarapper(result.getContents(), this);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        integrator.initiateScan();
    }

    //********************* camera **************************

    /**
     * @return
     */
    private boolean checkPermission() {
        // Permission is not granted
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                recreate();
                integrator = new IntentIntegrator(ScanAcitivy.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("scanning Soat item ...");
                integrator.setCameraId(0);
// Use a specific camera of the device
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();// `this` is the current Activity

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    showMessageOKCancel(
                            (dialog, which) -> requestPermission());
                }
            }
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ScanAcitivy.this)
                .setMessage("You need to allow Camera permissions ")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


}
